// https://leetcode.com/explore/featured/card/top-interview-questions-easy/127/strings/885/
function strStr(haystack, needle) {
  const pattern = new RegExp(needle, 'i');
  return pattern.test(haystack);
}

console.log(strStr('hello', 'll'));
console.log(strStr('aaaaaaaaa', 'bba'));
