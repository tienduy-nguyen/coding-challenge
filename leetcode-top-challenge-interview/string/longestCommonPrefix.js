function longestCommonPrefix(arr) {
  const arr1 = arr[0].split('');
  let prefix = '';

  for (let c of arr1) {
    const regex = new RegExp(`^${prefix + c}`, 'i');
    for (let i = 1; i < arr.length; ++i) {
      if (!regex.test(arr[i])) return prefix;
    }
    prefix += c;
  }
  return prefix;
}

function longestCommonPrefix2(arr) {
  let prefix = '';
  for (let i = 0; i < arr[0].length; i++) {
    for (let j = 1; j < arr.length; j++) {
      if (arr[0][i] !== arr[j][i]) return r;
    }
    prefix += arr[0][i];
  }

  return prefix;
}

console.log(longestCommonPrefix(['flower', 'flow', 'flight']));
console.log(longestCommonPrefix(['dog', 'racecar', 'car']));
