// https://leetcode.com/explore/featured/card/top-interview-questions-easy/127/strings/881/

function firstUniqueCharacter(str) {
  const count = {};
  const arr = str.split('');
  for (let c of arr) {
    count[c] ? count[c]++ : (count[c] = 1);
  }
  console.log(count);
  return Object.keys(count).findIndex((c) => count[c] === 1);
}

console.log(firstUniqueCharacter('leetcode'));
console.log(firstUniqueCharacter('loveleetcode'));
console.log(firstUniqueCharacter('ooo'));
