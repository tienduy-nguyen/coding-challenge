function validAnagram(str1, str2) {
  str1 = sortStr(str1);
  str2 = sortStr(str2);
  return str1 === str2;
}
function sortStr(str) {
  const arr = str.split('').sort((a, b) => {
    if (a > b) return 1;
    if (a < b) return -1;
    return 0;
  });
  return arr.join('');
}

console.log(validAnagram('anagram', 'nagaram'));
console.log(validAnagram('rat', 'car'));
