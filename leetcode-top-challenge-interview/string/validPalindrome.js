function validPalindrome(str) {
  str = str.replace(/\W/g, '').toLowerCase();
  const reverse = str.split('').reverse().join('');
  return str === reverse;
}

console.log(validPalindrome('A man, a plan, a canal: Panama'));
console.log(validPalindrome('race a car'));
