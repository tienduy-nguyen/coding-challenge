const str = 'sskfssbbb9bbb';
const str2 = 'abcdef';
const str3 = 'ssskffsssffbbbb9bbbb';

function SearchingChallenge(str) {
  const regexPattern = /(.{2,})(?=.*\1)/g; // loop group characters for at least 2 duplicated characters
  if (!regexPattern.test(str)) {
    return 'No pattern null';
  }
  const results = str.match(regexPattern);
  return (
    'yes ' +
    results.reduce((result, cur) => (result.length > cur.length ? result : cur))
  );
}

console.log(SearchingChallenge(str));
console.log(SearchingChallenge(str2));
console.log(SearchingChallenge(str3));
