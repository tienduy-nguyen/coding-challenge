function containDuplicate(arr) {
  const set = Array.from(new Set(arr));
  if (set.length < arr.length) return true;
  return false;
}
console.log(containDuplicate([1, 2, 3, 1]));
console.log(containDuplicate([1, 2, 3, 4]));
console.log(containDuplicate([1, 1, 1, 3, 3, 4, 3, 2, 4, 2]));
