// https://leetcode.com/explore/featured/card/top-interview-questions-easy/92/array/727/

// Remove duplicated from sorted Array
function removeDuplicate(arr) {
  const result = Array.from(new Set(arr));
  return result;
}

console.log(removeDuplicate([1, 1, 2]));
