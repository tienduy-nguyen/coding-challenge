// https://leetcode.com/problems/best-time-to-buy-and-sell-stock/

function bestTimeToBuy1(arr) {
  const len = arr.length;
  if (len < 2) return 0;
  let maxProfit = -Infinity;
  let minStock = arr[0];
  for (let price of arr) {
    minStock = Math.min(minStock, price);
    maxProfit = Math.max(maxProfit, price - minStock);
  }
  return maxProfit;
}

console.log(bestTimeToBuy1([3, 3, 5, 0, 0, 3, 1, 4]));
console.log(bestTimeToBuy1([7, 1, 5, 3, 6, 4]));
console.log(bestTimeToBuy1([1, 2, 3, 4, 5]));
console.log(bestTimeToBuy1([7, 6, 4, 3, 1]));

console.log('----------');

console.log(bestTimeToBuy1_2([3, 3, 5, 0, 0, 3, 1, 4]));
console.log(bestTimeToBuy1_2([7, 1, 5, 3, 6, 4]));
console.log(bestTimeToBuy1_2([1, 2, 3, 4, 5]));
console.log(bestTimeToBuy1_2([7, 6, 4, 3, 1]));
