// if we don't know how much time element appear
function singleNumber(arr) {
  let dic = {};
  for (let i of arr) {
    if (!dic[i]) {
      dic[i] = 1;
    } else {
      dic[i]++;
    }
  }
  return Object.keys(dic).find((i) => dic[i] === 1);
}

// If every element appears twice
function singleNumber2(arr) {
  arr.sort();
  for (let i = 0; i < arr.length; i += 2) {
    if (arr[i] != arr[i + 1]) return arr[i];
  }
}

console.log(singleNumber([2, 2, 1]));
console.log(singleNumber([4, 1, 2, 1, 2]));
console.log(singleNumber([1]));

console.log('-------------');
console.log(singleNumber2([2, 2, 1]));
console.log(singleNumber2([4, 1, 2, 1, 2]));
console.log(singleNumber2([1]));
