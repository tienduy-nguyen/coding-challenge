function arrSort(arr) {
  return arr.sort();
}

function sortObjectByName(arr) {
  return arr.sort((a, b) => {
    if (a.name > b.name) return 1;
    if (a.name < b.name) return -1;
    return 0;
  });
}

function sortObjectByAge(arr) {
  return arr.sort((a, b) => {
    if (a.age > b.age) return 1;
    if (a.age < b.age) return -1;
    return 0;
  });
}

const arr = [
  { name: 'd', age: 1 },
  { name: 'a', age: 3 },
  { name: 'c', age: 2 },
  { name: 'b', age: 1 },
];
console.log(sortObjectByName(arr));
console.log(sortObjectByAge(arr));
