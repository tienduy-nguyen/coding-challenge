// https://leetcode.com/explore/featured/card/top-interview-questions-easy/92/array/564/

function bestTimeToBuy2(arr) {
  const len = arr.length;
  if (len < 2) return 0;
  let result = 0;
  for (let i = 1; i < len; ++i) {
    result += Math.max(arr[i] - arr[i - 1], 0);
  }
  return result;
}

console.log(bestTimeToBuy2([3, 3, 5, 0, 0, 3, 1, 4]));
console.log(bestTimeToBuy2([7, 1, 5, 3, 6, 4]));
console.log(bestTimeToBuy2([1, 2, 3, 4, 5]));
console.log(bestTimeToBuy2([7, 6, 4, 3, 1]));
