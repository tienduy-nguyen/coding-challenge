function intersectArray(arr1, arr2) {
  const count = {};
  const result = [];
  arr1.forEach((i) => {
    count[i] ? count[i]++ : (count[i] = 1);
  });
  arr2.forEach((i) => {
    if (count[i]) {
      result.push(i);
      count[i]--;
    }
  });
  return result;
}

console.log(intersectArray([1, 2, 2, 1], [2, 2]));
console.log(intersectArray([4, 9, 5], [9, 4, 9, 8, 4]));
