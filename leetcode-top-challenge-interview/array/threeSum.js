function threeSum(arr, target) {
  const len = arr.length;
  let check = null;
  let newTarget = null;
  let count = 0;
  for (let i = 0; i < len - 3; ++i) {
    newTarget = target - arr[0];
    arr.splice(0, 1);
    count++;
    check = twoSum(arr, newTarget);
    if (check) return [i, count + check[0], count + check[1]];
  }
  return null;
}

function twoSum(arr, target) {
  const len = arr.length;
  arr.sort((a, b) => a - b);

  let left = 0;
  let right = len - 1;
  while (left < right) {
    const value = arr[left] + arr[right];
    if (value === target) return [left, right];
    value < target ? left++ : right--;
  }
  return null;
}

console.log(threeSum([2, 7, 11, 15], 20));
