function twoSum(target, arr) {
  const len = arr.length;
  arr.sort((a, b) => a - b);

  let left = 0;
  let right = len - 1;
  while (left < right) {
    const value = arr[left] + arr[right];
    if (value === target) return [left, right];
    value < target ? left++ : right--;
  }
  return null;
}

console.log(twoSum(9, [2, 7, 11, 15]));
