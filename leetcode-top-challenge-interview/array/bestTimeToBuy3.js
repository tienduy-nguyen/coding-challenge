// https://leetcode.com/problems/best-time-to-buy-and-sell-stock-iii/

function bestTimeToBuy3(arr) {
  const len = arr.length;
  if (len < 2) return 0;
  let firstBuy = -Infinity;
  let firstSell = 0;
  let secondBuy = -Infinity;
  let secondSell = 0;
  let i = 0;
  console.log(`day |price |firstBuy |firstSell |secondBuy |secondSell`);
  for (let price of arr) {
    firstBuy = Math.max(firstBuy, -price);
    firstSell = Math.max(firstSell, firstBuy + price);
    secondBuy = Math.max(secondBuy, firstSell - price);
    secondSell = Math.max(secondSell, secondBuy + price);

    console.log(
      `${i}   |${price}     |${firstBuy}       |${firstSell}         |${secondBuy}        |${secondSell}`
    );
    i++;
  }
  return secondSell;
}

console.log(bestTimeToBuy3([3, 3, 5, 0, 0, 3, 1, 4]));
console.log(bestTimeToBuy3([1, 2, 3, 4, 5]));
