function moveZero(arr) {
  const zero = arr.filter((i) => i === 0);
  return arr.filter((i) => i !== 0).concat(zero);
}

console.log(moveZero([0, 1, 0, 3, 12]));
