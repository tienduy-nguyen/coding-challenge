//https://leetcode.com/explore/featured/card/top-interview-questions-easy/92/array/646/
// Given an array, rotate the array to the right by k steps, where k is non-negative.
function rotateArray(k, arr) {
  const len = arr.length;
  if (k >= len) return arr;
  const first = arr.slice(0, len - k);
  const second = arr.slice(len - k, len);
  return second.concat(first);
}

function rotateArray2(k, arr) {
  const len = arr.length;
  if (k >= len) return arr;
  const first = arr.splice(0, len - k);
  return arr.concat(first);
}

console.log(rotateArray(3, [1, 2, 3, 4, 5, 6, 7]));
console.log(rotateArray(2, [-1, -100, 3, 99]));

console.log(rotateArray2(3, [1, 2, 3, 4, 5, 6, 7]));
console.log(rotateArray2(2, [-1, -100, 3, 99]));
