function plusOne(digits) {
  return (Number(digits.join('')) + 1).toString().split('');
}
console.log(plusOne([1, 2, 3]));
