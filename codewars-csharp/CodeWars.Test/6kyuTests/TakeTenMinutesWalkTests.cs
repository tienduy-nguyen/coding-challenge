﻿using System;
using NUnit.Framework;
using Codewars._6kyu;
namespace Codewars.Test._6kyuTests
{
    class TakeTenMinutesWalkTests
    {
       [Test]
        public void Test()
        {
            Assert.AreEqual(true, TakeTenMinutesWalk.isValid(new char[] { 'n', 's', 'n', 's', 'n', 's', 'n', 's', 'n', 's' }));
            Assert.AreEqual(false, TakeTenMinutesWalk.isValid(new char[] { 'w', 'e', 'w', 'e', 'w', 'e', 'w', 'e', 'w', 'e', 'w', 'e' }));
            Assert.AreEqual(false, TakeTenMinutesWalk.isValid(new char[] { 'w' }));
            Assert.AreEqual(false, TakeTenMinutesWalk.isValid(new char[] { 'n', 'n', 'n', 's', 'n', 's', 'n', 's', 'n', 's' }));
        }
    }
