﻿using System.Collections.Generic;
using NUnit.Framework;
using Codewars._6kyu;

namespace Codewars.Test._6kyuTests
{
    class ClimbersRankingsTests
    {
        [Test]
        public void SimpleTest()
        {
            var rankings = new Dictionary<string, IEnumerable<int>>
            {
                {"SKOFIC Domen", new[] {55, 100, 100, 25, 100, 51, 80}}
            };
            var result = ClimbersRankings.GetRankings(rankings);

            Assert.AreEqual(486, result["SKOFIC Domen"]);
        }
    }
}
