﻿using System;
using NUnit.Framework;
using Codewars._6kyu;

namespace Codewars.Test._6kyuTests
{
    class NSmallestElementsInOriginalOrderTests
    {
        [Test]
        public void FixedTest()
        {
            Assert.AreEqual(new[] { 1, 2, 3 }, NSmallestElementsInOriginalOrder.FirstNSmallest(new[] { 1, 2, 3, 4, 5 }, 3));
            Assert.AreEqual(new[] { 0, 2, 1 }, NSmallestElementsInOriginalOrder.FirstNSmallest(new[] { 5, 4, 0, 2, 1 }, 3));
            Assert.AreEqual(new[] { 1, 2, 1 }, NSmallestElementsInOriginalOrder.FirstNSmallest(new[] { 1, 2, 3, 1, 2 }, 3));
            Assert.AreEqual(new[] { 1, -4, 0 }, NSmallestElementsInOriginalOrder.FirstNSmallest(new[] { 1, 2, 3, -4, 0 }, 3));
            Assert.AreEqual(new int[0], NSmallestElementsInOriginalOrder.FirstNSmallest(new[] { 1, 2, 3, 4, 5 }, 0));
            Assert.AreEqual(new[] { 1, 2, 3, 4, 5 }, NSmallestElementsInOriginalOrder.FirstNSmallest(new[] { 1, 2, 3, 4, 5 }, 5));
            Assert.AreEqual(new[] { 1, 2, 3, 2 }, NSmallestElementsInOriginalOrder.FirstNSmallest(new[] { 1, 2, 3, 4, 2 }, 4));
            Assert.AreEqual(new[] { 2, 1 }, NSmallestElementsInOriginalOrder.FirstNSmallest(new[] { 2, 1, 2, 3, 4, 2 }, 2));
            Assert.AreEqual(new[] { 2, 1, 2 }, NSmallestElementsInOriginalOrder.FirstNSmallest(new[] { 2, 1, 2, 3, 4, 2 }, 3));
            Assert.AreEqual(new[] { 2, 1, 2, 2 }, NSmallestElementsInOriginalOrder.FirstNSmallest(new[] { 2, 1, 2, 3, 4, 2 }, 4));
        }

    }
}