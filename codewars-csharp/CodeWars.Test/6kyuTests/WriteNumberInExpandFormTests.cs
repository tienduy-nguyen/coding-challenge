﻿using NUnit.Framework;
using Codewars._6kyu;

namespace Codewars.Test._6kyuTests
{
    class WriteNumberInExpandFormTests
    {
        [TestFixture]
        public class SolutionTest
        {
            [Test]
            public void BasicTests()
            {
                Assert.That(WriteNumberInExpandForm.ExpandedForm(12), Is.EqualTo("10 + 2"));
                Assert.That(WriteNumberInExpandForm.ExpandedForm(42), Is.EqualTo("40 + 2"));
                Assert.That(WriteNumberInExpandForm.ExpandedForm(70304), Is.EqualTo("70000 + 300 + 4"));
            }
        }
    }
}
