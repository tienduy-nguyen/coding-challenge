﻿using System;
using NUnit.Framework;
using Codewars._6kyu;

namespace Codewars.Test._6kyuTests
{
    class DeTectPangramTests
    {
        [Test]
        public void SampleTests()
        {
            Assert.AreEqual(true, DetectPangram.IsPangram("The quick brown fox jumps over the lazy dog."));
        }
        [Test]
        public void Test2()
        {
            Assert.AreEqual(false, DetectPangram.IsPangram("abcdefghijklmopqrstuvwxyz "));
        }
        [Test]
        public void Test3()
        {
            Assert.AreEqual(true, DetectPangram.IsPangram("ABCD45EFGH,IJK,LMNOPQR56STUVW3XYZ"));
        }
        [Test]
        public void Test4()
        {
            Assert.AreEqual(true, DetectPangram.IsPangram("Raw Danger! " +
                                                          "(Zettai Zetsumei Toshi 2) for the PlayStation 2 is a " +
                                                          "bit queer, but an alright game I guess, uh... CJ kicks and vexes " +
                                                          "Tenpenny precariously? This should be a pangram now, probably."));
        }

    }
}
