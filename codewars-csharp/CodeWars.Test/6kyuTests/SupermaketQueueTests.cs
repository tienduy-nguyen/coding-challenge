﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Codewars._6kyu;
namespace Codewars.Test._6kyuTests
{
    [TestFixture]
    class SupermaketQueueTests
    {
        [Test]
        public void FixedTest()
        {
            Assert.AreEqual(0, SupermaketQueue.QueueTime(new int[] { }, 1));
            Assert.AreEqual(10, SupermaketQueue.QueueTime(new int[] { 1, 2, 3, 4 }, 1));
            Assert.AreEqual(9, SupermaketQueue.QueueTime(new int[] { 2, 2, 3, 3, 4, 4 }, 2));
            Assert.AreEqual(5, SupermaketQueue.QueueTime(new int[] { 1, 2, 3, 4, 5 }, 100));
            Assert.AreEqual(12, SupermaketQueue.QueueTime(new int[] { 5, 3, 4 }, 1));
            Assert.AreEqual(10, SupermaketQueue.QueueTime(new int[] { 10, 2, 3, 3 }, 2));
            Assert.AreEqual(12, SupermaketQueue.QueueTime(new int[] { 2, 3, 10 }, 2));
            Assert.AreEqual(11, SupermaketQueue.QueueTime(new int[] { 2, 3, 10,5,8 }, 3));
            Assert.AreEqual(13, SupermaketQueue.QueueTime(new int[] { 2,2,1,3,7,6, 3, 10 }, 4));
        }
        [Test]
        public void RandomTests()
        {
            Random rand = new Random();

            Func<int[], int, long> Solution = (customers, n) =>
            {
                if (!customers.Any())
                {
                    return 0;
                }

                Queue<int> customerTimes = new Queue<int>(customers);
                List<long> tills = new List<long>(n);
                tills.AddRange(Enumerable.Repeat(0, n).Select(i => (long)i));

                while (customerTimes.Count > 0)
                {
                    tills[tills.IndexOf(tills.Min())] += customerTimes.Dequeue();
                }

                return tills.Max();
            };

            for (int i = 0; i < 100; i++)
            {
                int length = rand.Next(0, 150);

                var customers = Enumerable.Range(0, length).Select(e => rand.Next(1, 21)).ToArray();

                int tillCount = rand.Next(1, 11);

                long expected = Solution(customers, tillCount);

                long actual = SupermaketQueue.QueueTime(customers, tillCount);

                Assert.AreEqual(expected, actual);
            }
        }
    }
}
