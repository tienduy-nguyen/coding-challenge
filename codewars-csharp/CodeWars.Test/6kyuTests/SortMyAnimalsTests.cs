﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Codewars._6kyu;

namespace Codewars.Test._6kyuTests
{
    [TestFixture]
    public class SortMyAnimalsTests
    {
        [Test]
        public void SortTest()
        {
            var animals = new List<SortMyAnimals.Animal>
            {
                new SortMyAnimals.Animal {Name = "Cat", NumberOfLegs = 4},
                new SortMyAnimals.Animal {Name = "Snake", NumberOfLegs = 0},
                new SortMyAnimals.Animal {Name = "Dog", NumberOfLegs = 4},
                new SortMyAnimals.Animal {Name = "Pig", NumberOfLegs = 4},
                new SortMyAnimals.Animal {Name = "Human", NumberOfLegs = 2},
                new SortMyAnimals.Animal {Name = "Bird", NumberOfLegs = 2}
            };
            var output = new SortMyAnimals.AnimalSorter().Sort(animals);
            Assert.AreEqual(output[0].Name, "Snake");
            Assert.AreEqual(output[3].Name, "Cat");
            var output2 = new SortMyAnimals.AnimalSorter().Sort(null);
            Assert.AreEqual(null, actual: output2);
        }
    }
}
