﻿using System.Collections.Generic;
using NUnit.Framework;
using Codewars._5kyu;

namespace Codewars.Test._5kyuTests
{
    [TestFixture]
    public class NotVerySecureTests
    {
        private static IEnumerable<TestCaseData> testCases
        {
            get
            {
                yield return new TestCaseData("Mazinkaiser").Returns(true);
                yield return new TestCaseData("hello world_").Returns(false);
                yield return new TestCaseData("PassW0rd").Returns(true);
                yield return new TestCaseData("     ").Returns(false);
            }
        }

        [Test, TestCaseSource("testCases")]
        public bool Test(string str) => NotVerrySecure.Alphanumeric(str);
    }
}
