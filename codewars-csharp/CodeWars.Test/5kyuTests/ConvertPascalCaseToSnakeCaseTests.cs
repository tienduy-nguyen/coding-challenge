﻿using System;
using NUnit.Framework;
using Codewars._5kyu;

namespace Codewars.Test._5kyuTests
{
    class ConvertPascalCaseToSnakeCaseTests
    {
        [Test]
        public void SampleTests()
        {
            Assert.AreEqual("test_controller", ConvertPascalCaseToSnakeCase.ToUnderscore("TestController"));
            Assert.AreEqual("this_is_beautiful_day", ConvertPascalCaseToSnakeCase.ToUnderscore("ThisIsBeautifulDay"));
            Assert.AreEqual("am7_days", ConvertPascalCaseToSnakeCase.ToUnderscore("Am7Days"));
            Assert.AreEqual("my3_code_is4_times_better", ConvertPascalCaseToSnakeCase.ToUnderscore("My3CodeIs4TimesBetter"));
            Assert.AreEqual("arbitrarily_sending_different_types_to_functions_makes_katas_cool", ConvertPascalCaseToSnakeCase.ToUnderscore("ArbitrarilySendingDifferentTypesToFunctionsMakesKatasCool"));
            Assert.AreEqual("1", ConvertPascalCaseToSnakeCase.ToUnderscore(1), "Numbers should be turned to strings!");
        }
    }

}

