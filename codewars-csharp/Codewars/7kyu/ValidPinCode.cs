﻿using System;
using System.Text.RegularExpressions;

namespace Codewars.Sixkyu
{
	//Solution using regex
    public class ValidPinCode
    {
        public static bool ValidatePin(string pin)
        {
            return Regex.IsMatch(pin, @"^((\d{6})|(\d{4}))$");
        }
    }
	
	//Normal solution + Learn to organise the method
	 public class SolutionChar
    {
        private static string _pin;


        private SolutionChar(string pin)
        {
            _pin = pin;
        }

        private bool IsValidLength()
        {
            int length = _pin.Length; ;
            if (length == 4 || length == 6)
            {
                return  true ;
            }
            return  false;

        }

        private  bool IsValidDigit()
        {
            char[] pinChars = _pin.ToCharArray();

            foreach (var itemChar in pinChars)
            {
                if (!Char.IsDigit(itemChar))
                {
                    return  false;
                }
            }
            return  true;
        }

        private bool IsValidAll()
        {
            bool v1;
            bool v2;
            v1 = IsValidLength() ;
            v2 = IsValidDigit() ;
            if( v1 && v2)
            {
                return true;
            }
            return false;
        }


        public static bool ValidatePin(string pin)
        {
            var solution = new SolutionChar(pin);
            bool result;
            result = solution.IsValidAll() ;

            return result;
        }

    }
}
