//Best practice
//Solution of liang1403, KrolikDovakin

using System;
using System.Linq;

public class Kata
{
  public static string FirstNonRepeatingLetter(string s)
  {
    var ret = s.GroupBy(z => char.ToLower(z)).Where(g => g.Count() == 1).FirstOrDefault();
    return (ret != null) ? ret.First().ToString() : string.Empty;
  }
}

//Other solution
using System;
using System.Linq;

public class Kata
{
  public static string FirstNonRepeatingLetter(string s)
  {
    return s.GroupBy(char.ToLower)
            .Where(gr => gr.Count() == 1)
            .Select(x => x.First().ToString())
            .DefaultIfEmpty("")
            .First();
  }
}