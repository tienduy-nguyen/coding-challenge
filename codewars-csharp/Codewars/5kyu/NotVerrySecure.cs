﻿using System;
using System.Text.RegularExpressions;

namespace Codewars._5kyu
{
    public class NotVerrySecure
    {
        public static bool Alphanumeric(string str)
        {
            return Regex.IsMatch(str, @"(?:^([A-Za-z0-9]+)$)");
        }
    }
}
