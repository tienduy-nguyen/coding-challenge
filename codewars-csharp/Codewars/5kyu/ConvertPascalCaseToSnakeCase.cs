﻿using System;
using System.Text.RegularExpressions;

namespace Codewars._5kyu
{
    public class ConvertPascalCaseToSnakeCase
    {
        public static string ToUnderscore(string str)
        {
            return Regex.Replace(str, @"\B[A-Z]\B", s => $"_{s.Value}").ToLower();
        }
        public static string ToUnderscore(int input)
        {
            return ToUnderscore(input.ToString());
        }
    }
}
