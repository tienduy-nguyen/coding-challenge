﻿using System;
using System.Linq;


namespace Codewars._5kyu
{
    public class PerfectPower
    {
        public static (int, int)? IsPerfectPower(int n)
        {
            //var numRange = Enumerable.Range(2, n).ToArray();
            if (n < 94645976)
            {
                for (int x = 2; x * x <= n; ++x)
                {
                    for (int y = 2; Math.Pow(x, y) <= n; ++y)
                    {
                        if (Math.Pow(x, y) == n)
                            return (x, y);
                    }
                    //int y = numRange.FirstOrDefault(item => Math.Pow(x, item) == n);
                    //if (y >= 2) return (x, y);
                }
            }
            return null;
        }

        static void Main(string[] args)
        {
            int a = 3;
            int n = 8;
            var numRange = Enumerable.Range(2, n).ToArray();
            int result = numRange.First(x => Math.Pow(a, x) == n);
            //var result2 = numRange.Select(x => numRange.Where(y => Math.Pow(x, y) == n).Select(z => z);


            Console.WriteLine(a + " " + result);
            Console.ReadKey();
        }
    }
}
