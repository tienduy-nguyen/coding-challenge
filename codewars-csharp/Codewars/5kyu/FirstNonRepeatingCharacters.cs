﻿using System;
using System.Linq;


namespace Codewars._5kyu
{
    public class FirstNonRepeatingCharacters
    {
        public static string FirstNonRepeatingLetter(string s)
        {
            var ret = s.GroupBy(z => char.ToLower(z)).Where(g => g.Count() == 1).FirstOrDefault();
            return (ret != null) ? ret.First().ToString() : string.Empty;
        }
        public static string FirstNonRepeatingLetter2(string s)
        {
            char result = s.FirstOrDefault(
                x => s.ToLower().IndexOf(char.ToLower(x)) ==
                     s.ToLower().LastIndexOf(char.ToLower(x)));
            if (result != '\0')
                return result.ToString();
            return string.Empty;
        }
        public static string FirstNonRepeatingLetterDuy(string s)
        {
            char result = s.FirstOrDefault(
                x => s.ToLower().IndexOf(x.ToString().ToLower(), StringComparison.Ordinal) ==
                     s.ToLower().LastIndexOf(x.ToString().ToLower(), StringComparison.Ordinal));
            if (result != '\0')
                return result.ToString();
            return string.Empty;
        }
    }
}
