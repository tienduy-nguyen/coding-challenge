﻿using System;



namespace Codewars._6kyu
{
    public class SupermaketQueue
    {
        public static long QueueTime(int[] customers, int n)
        {
            int[] result = new int[n];
            int cusLen = customers.Length;
            for (int i = 0; i < cusLen; i++)
            {
                result[0] += customers[i];
                Array.Sort(result);
            }
            return result[n - 1];
        }
        public static void Main (string [] args)
        {
            int[] customers = new[] { 2, 2, 1, 3, 7, 6, 3, 10 };
            int n = 4;

            int[] result = new int[n];
            for (int i = 0; i < customers.Length; i++)
            {
                result[0] += customers[i];
                Array.Sort(result);
            }
            Console.WriteLine(result[n-1]);
            Console.ReadKey();
        }
    }
}
