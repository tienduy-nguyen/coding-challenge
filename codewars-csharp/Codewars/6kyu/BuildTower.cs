﻿using System;


namespace Codewars._6kyu
{
    public class BuildTower
    {
        public static string[] TowerBuilder(int nFloors)
        {
            string[] results = new string[nFloors];
            for (int i = 0; i < nFloors; i++)
            {
                results[i] = new string(' ',nFloors-i-1) + new string('*', (i + 1) * 2 - 1) + new string(' ', nFloors-i-1);
            }
            return results;
        }
    }
}
