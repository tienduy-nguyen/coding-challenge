﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Codewars._6kyu
{
    public class RomanConvert
    {

        public static string Solution(int n)
        {
            int value = 0;
            var result = new StringBuilder();
            var dicRoman = new Dictionary<string, int>
            {
                {"M", 1000}, {"CM", 900}, {"D", 500}, {"CD", 400}, {"C", 100},
                {"XC", 90}, {"L", 50}, {"XL", 40}, {"X", 10}, {"IX", 9}, {"V", 5}, {"IV", 4}, {"I", 1}
            };

            foreach (var item in dicRoman)
            {
                while (value + item.Value <= n)
                {
                    value += item.Value;
                    result.Append(item.Key);
                }
            }
            return result.ToString();
        }
    }
}

