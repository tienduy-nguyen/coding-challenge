﻿namespace Codewars._6kyu
{

    public class BuildAPileCubes
    {
        public static long findNb(long m)
        {
            if (m <= 1)
                return -1;

            long sum = 0;
            long  i = 0;
            while (sum <m)
            {
                i++;
                sum += i*i*i;
                if (sum == m)
                    return i;
            }
            return -1;
        }

    }
}
