﻿using System;
using System.Linq;


namespace Codewars._6kyu
{
    public class EqualSidesOfAnArray
    {
        public static int FindEvenIndex(int[] arr)
        {
            if (arr.Length == 0 || arr.Length > 1000)
                return -1;
            int sumArr = arr.Sum();
            if (sumArr == 0 ||sumArr == arr[0])
                return 0;
            int value = 0;
            for (int i = 0; i < arr.Length-1; i++)
            {
                value += arr[i];
                if (value*2 == (sumArr - arr[i + 1]))
                    return i + 1;
            }
            return -1;
        }
    }
}
