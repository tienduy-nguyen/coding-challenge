﻿using System.Collections.Generic;
using System.Linq;

namespace Codewars._6kyu
{
    public class UniqueOrder
    {
        public static IEnumerable<T> UniqueInOrder<T>(IEnumerable<T> iterable)
        {
            //your code here...
            if(!iterable.Any())
                return Enumerable.Empty<T>();
            var results = new List<T>();
            int i = 0;
            results.Add(iterable.ElementAtOrDefault(0));
            foreach (T str in iterable)
            {
                if (!Equals(str,results[i]))
                {
                    results.Add(str);
                    i++;
                }
            }
            return results;
        }
    }
}
