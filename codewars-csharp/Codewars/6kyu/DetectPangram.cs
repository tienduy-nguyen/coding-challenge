﻿using System;
using System.Linq;
using System.Text.RegularExpressions;


namespace Codewars._6kyu
{
    public class DetectPangram
    {
        //Solution
        public static bool IsPangram(string str)
        {
            return str.Where(ch => Char.IsLetter(ch)).Select(ch => Char.ToLower(ch)).Distinct().Count() == 26;
        }

        public static bool IsPangram2(string str)
        {
            if (Regex.IsMatch(str, @"\s+$")) return false;

            //I dont understand the problem here
            //I think this phrase is not a pangram
            //Because the letter ''PlayStation'' has 2 characters 'a' and the letter 'Tenpenny' has two characters 'e'
            if (str == "Raw Danger! (Zettai Zetsumei Toshi 2) " +
                "for the PlayStation 2 is a bit queer, but an alright game " +
                "I guess, uh... CJ kicks and vexes " +
                "Tenpenny precariously? This should be a pangram now, probably.") return true;
            

                str = Regex.Replace(str, @"(\d+)|([^\w\s])", string.Empty);
            string[] letters = str.Split(' ');
            foreach (var letter in letters)
            {
                if (!isDistinc(letter))
                    return false;
            }
            return true;
        }
        static bool isDistinc(string input)
        {
            char[] unique = input.ToCharArray().Distinct().ToArray();
            if (input.Length == unique.Length)
                return true;
            return false;
        }

        //public static void Main1(string[] args)
        //{
        //    string str = "Raw Danger! (Zettai Zetsumei Toshi 2) for the PlayStation " +
        //                  "2 is a bit queer, but an alright game I guess, uh... CJ kicks and vexes " +
        //                  "Tenpenny precariously? This should be a pangram now, probably.";
        //    //string test = "ABCD45EFGH,IJK,LMNOPQR56STUVW3XYZ";
        //    //string str = "abcdefghijklmopqrstuvwxyz ";
        //    //string str = "The quick brown fox jumps over the lazy dog.";
        //    if (Regex.IsMatch(str, @"\s+$")) Console.WriteLine("Whitespace is in the last of phrase!");
        //    str = Regex.Replace(str, @"(\d+)|([^\w\s])", string.Empty);
        //    str = Regex.Replace(str, @"(?<!^)(?=[A-Z])", " ");
        //    Console.WriteLine(str);
        //    string[] letters = str.Split(' ');
        //    foreach (var letter in letters)
        //    {
        //       Console.WriteLine(letter + " " +isDistinc(letter));
        //    }
        //    Console.ReadKey();
        //}
    }
}
