﻿using System;

namespace Codewars._6kyu
{
    public class BitCounting
    {
        public static int CountBits(int n)
        {
            int count = 0;
            while (n != 0)
            {
                count++;
                n &= (n - 1);
            }
            return count;
        }
    }
}
