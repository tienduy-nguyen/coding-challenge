using System.Collections.Generic;
using System.Linq;

namespace ClimbersRanking
{
    public static class RankCalculator
    {
        public static Dictionary<string, int> GetRankings(Dictionary<string, IEnumerable<int>> pointsByClimber)
        {            
          return pointsByClimber.OrderByDescending(a => a.Value.OrderByDescending(c => c).Take(6).Sum()).ToDictionary(p => p.Key, p => p.Value.OrderByDescending(c => c).Take(6).Sum());
        }
    }
}




using System;
using System.Collections.Generic;
using System.Linq;

namespace ClimbersRanking
{
    public static class RankCalculator
    {
        public static Dictionary<string, int> GetRankings(Dictionary<string, IEnumerable<int>> pointsByClimber)
        {
            // todo: should return rankings by climber, where best 6 results for climber count... 
            // todo:   return them in descending order (the one with most points is first)
            // todo:   returned collection format: string Name Surname, int Sum of counting points
            return pointsByClimber
                .ToDictionary(x => x.Key, x => x.Value.OrderByDescending(y => y).Take(6).Sum())
                .OrderByDescending(x => x.Value)
                .ToDictionary(x => x.Key, x => x.Value);
        }
    }
}




using System;
using System.Collections.Generic;
using System.Linq;

namespace ClimbersRanking
{
    public static class RankCalculator
    {
        public static Dictionary<string, int> GetRankings(Dictionary<string, IEnumerable<int>> pointsByClimber)
        {
            return pointsByClimber
                .Select(e => Tuple.Create(e.Key, e.Value.OrderByDescending(p => p).Take(6).Sum()))
                .OrderByDescending(e => e.Item2)
                .ToDictionary(e => e.Item1, e => e.Item2);
        }
    }
}