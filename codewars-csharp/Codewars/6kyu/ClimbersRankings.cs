﻿using System;
using System.Collections.Generic;
using System.Linq;



namespace Codewars._6kyu
{
    public class ClimbersRankings
    {
        public static Dictionary<string, int> GetRankings(Dictionary<string, IEnumerable<int>> pointsByClimber)
        {
            return pointsByClimber.ToDictionary(x => x.Key, y => y.Value.OrderByDescending(z => z).Take(6).Sum())
                                    .OrderByDescending(z  => z.Value).ToDictionary(x => x.Key, y => y.Value);

        }

        public static void Main(string[] args)
        {
            var rankings = new Dictionary<string, IEnumerable<int>>
            {
                {"SKOFIC Domen", new[] {55, 100, 100, 25, 100, 51, 80}},
                {"Mike TheSecond", new[] {20, 22, 24, 26, 28, 30, 100, 100, 100}}
            };

                var result3 = rankings.ToDictionary(x=> x.Key, 
                                  y => y.Value.OrderByDescending(z => z).Take(6).Sum()).OrderByDescending(z => z.Value);
         
            //Console.WriteLine(result["SKOFIC Domen"]);
            Console.WriteLine(result3);
            Console.ReadKey();
        }
    }
}
