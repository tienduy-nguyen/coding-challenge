﻿using System.Linq;

namespace Codewars._6kyu
{
    public class SplitString
    {
        public static string[] Solution(string str)
        {

            if (str.Length == 0)
                return null;
            
            if (str.Length % 2 != 0)
            {
                str = str + "_";
            }
            return Enumerable.Range(0, str.Length / 2)
                .Select(i => str.Substring(i * 2, 2)).ToArray();
        }
    }
}
