﻿using System.Linq;

namespace Codewars._6kyu
{
    public class FindTheDevisors
    {
        public static int[] Divisors(int n)
        {
            if (n <= 2)
                return null;

            //List<int> results = new List<int>();
            //for (int i = 2; i < n; i++)
            //{
            //    if (n % i == 0)
            //    {
            //        results.Add(i);
            //    }
            //}
            //int[] finish = results.ToArray();
            //return (finish.Length == 0) ? null : finish;

            ////Solution using Linq but I have a trouble with a System.OutOfMemoryException when the input int too big
            try
            {
                var listNumbers = Enumerable.Range(2, n - 1).ToArray();
                var results = listNumbers?.Where(num => (n % num == 0 && num < n))?.ToArray();
                if (results is null || results.Length == 0)
                {
                    return null;
                }

                return results;
            }
            catch
            {
                return null;
            }
        }
    }
}
