﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Codewars._6kyu
{
    public class IQ
    {
        public static int Test(string numbers)
        {
            List<string> listNumbers = numbers.Split(' ').ToList();

            var oddNumbers = listNumbers.Where(num => (Int32.Parse(num))%2 != 0).ToList();
            var evenNumbers  = listNumbers.Where(num => (Int32.Parse(num))%2 == 0).ToList();
            var firstOdd = oddNumbers.FirstOrDefault();
            var firstEven = evenNumbers.FirstOrDefault();
            return oddNumbers.Count <= evenNumbers.Count
                ? (listNumbers.IndexOf(firstOdd)+1)
                : (listNumbers.IndexOf(firstEven)+1);
        }
    }
}
    


