﻿using System.Linq;

namespace Codewars._6kyu
{
    public class WhoLikeIt
    {
        public static string Likes(string[] name)
        {
            if (name.Length == 1 && !string.IsNullOrWhiteSpace(name.FirstOrDefault()))
            {
                return $"{name.FirstOrDefault()} likes this";
            }
            if (name.Length == 2)
            {
                return $"{name.FirstOrDefault()} and {name.LastOrDefault()} like this";
            }
            if (name.Length == 3)
            {
                return $"{name.FirstOrDefault()}, {name.ElementAt(1)} and {name.LastOrDefault()} like this";
            }
            if (name.Length >= 3)
            {
                return $"{name.FirstOrDefault()}, {name.ElementAt(1)} and {name.Length - 2} others like this";
            }

            return "no one likes this";

        }
    }
}
