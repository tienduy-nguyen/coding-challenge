﻿using System;
using System.Linq;


namespace Codewars._6kyu
{
    public class NSmallestElementsInOriginalOrder
    {
        public static int[] FirstNSmallest(int[] arr, int n)
        {
            var smallestNumbers = arr.OrderBy(x => x).Take(n).ToList();
            return arr.Where(x => smallestNumbers.Remove(x)).ToArray();
        }

        public static int[] FirstNSmallest1(int[] arr, int n)
        {
            int arrLen = arr.Length;
            if (n <= 0 || arrLen == 0)
                return new int[0];
            if (n > arrLen) n = arrLen;
            var listSorted = arr.OrderBy(x => x).ToList().GetRange(0, n < arrLen ? n + 1 : arrLen);
            var result =  arr.Where(x => x <= n).Where(x => listSorted.Contains(x)).ToList();
            var sizeList = result.Count;
            var max = result.Max();
            while (sizeList > n)
            {
                for (var j = result.Count - 1; j >= 0; j--)
                {
                    if (result[j] != max) continue;
                    result.RemoveAt(j);
                    break; ;
                }
                sizeList = result.Count;
                max = result.Max();
            }
            return result.ToArray();
        }

        static void Main(string[] args)
        {
            var arr = new[] { 5, 4, 0, 2, 1 };
            int n = 3;
            int arrLen = arr.Length;
            if (n > arrLen) n = arrLen;
            var listSorted = arr.OrderBy(x => x).ToList().GetRange(0, n);
            var result = arr.Where(x => x <= n).Where(x => listSorted.Contains(x)).ToList();
            var smallestNumbers = arr.OrderBy(x => x).Take(n).ToList();
            var fd= arr.Where(x => smallestNumbers.Remove(x)).ToArray();
            var sizeList = result.Count;
            var max = result.Max();
            while (sizeList > n)
            {
                for (var j = result.Count - 1; j >= 0; j--)
                {
                    if (result[j] != max) continue;
                    result.RemoveAt(j);
                    break; ;
                }
                sizeList = result.Count;
                max = result.Max();
            }
            Console.WriteLine(result);
            foreach (var VARIABLE in result)
            {
                Console.WriteLine(VARIABLE);
            }
            Console.ReadKey();
        }
        

        
    }

}
