﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Codewars._6kyu
{
    public class WriteNumberInExpandForm
    {
        public static string ExpandedForm(long num)
        {
            List<string> list = new List<string>();
            int numLen = num.ToString().Length;

            char[] numChars = num.ToString().ToCharArray();
            for (int i = 0; i < numLen; i++)
            {
                if (numChars[i] != '0')
                    list.Add(numChars[i].ToString() + new string('0',numLen-i-1));
            }

            return string.Join(" + ", list);
        }

        public static class Kata
        {
            public static string ExpandedForm(long num)
            {
                var str = num.ToString();
                return string.Join(" + ", str
                    .Select((x, i) => char.GetNumericValue(x) * Math.Pow(10, str.Length - i - 1))
                    .Where(x => x > 0));
            }
        }
}
}
