function validParentheses(str) {
  const pattern = /\[\]|\{\}|\(\)/g;

  let len1 = str.length;
  let len2 = 0;

  while (len2 < len1 && str.length) {
    len1 = str.length;
    str = str.replace(pattern, '');
    len2 = str.length;
  }
  return !str.length;
}

console.log(validParentheses('((([{[[[({})]]]}])))'));
console.log(validParentheses('()[]{}'));
console.log(validParentheses('([)]'));
