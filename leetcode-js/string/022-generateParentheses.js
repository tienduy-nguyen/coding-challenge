function generateParentheses(n) {
  const res = [];

  // Left: remaining, r: right remaining
  function compose(left, right, str) {
    if (left > right) return; // The number of '(' should  be always >= ')'
    if (!left && !right) return res.push(str);
    console.log(`left-right: ${left} - ${right} - str: ${str} - res: ${res}`);

    if (left > 0) compose(left - 1, right, str + '(');
    if (right > 0) compose(left, right - 1, str + ')');
  }

  compose(n, n, '');
  return res;
}
// console.log(generateParentheses(3));
console.log(generateParentheses(3));
