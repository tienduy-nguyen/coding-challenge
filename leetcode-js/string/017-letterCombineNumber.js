function letterCombineNumber(digits) {
  const map = {
    2: ['a', 'b', 'c'],
    3: ['d', 'e', 'f'],
    4: ['g', 'h', 'i'],
    5: ['j', 'k', 'l'],
    6: ['m', 'n', 'o'],
    7: ['p', 'q', 'r', 's'],
    8: ['t', 'u', 'v'],
    9: ['w', 'x', 'y', 'z'],
  };
  const len = digits.length;

  if (len <= 0) return [];
  let combinations = [];
  for (let i of digits.slice(1, len)) {
    combinations.push(map[i]);
  }
  return combinations.reduce((result, arr) => {
    result = result.flatMap((d) => arr.map((v) => d + v));
    return result;
  }, map[digits[0]]);
}

console.log(letterCombineNumber('23'));
console.log(letterCombineNumber('234'));
