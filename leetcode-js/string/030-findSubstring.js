// https://leetcode.com/problems/substring-with-concatenation-of-all-words/

const findSubstring = (str, words) => {
  if (!words || words.length <= 0) return [];

  const m = words.length,
    n = words[0].length,
    len = m * n,
    result = [];
  let map = {};
  for (let w of words) map[w] = ~~map[w] + 1;

  for (let i = 0; i < str.length - len + 1; ++i) {
    const temp = Object.assign({}, map);
    for (let j = i; j < i + len; j += n) {
      const sub = str.substr(j, n);

      if (!(sub in temp)) break;

      if (--temp[sub] === 0) delete temp[sub];
    }

    if (Object.keys(temp).length === 0) result.push(i);
  }
  return result;
};

function findSubstring3(str, words) {
  const len = words.length;
  const result = [];
  if (len < 1) return [];
  const combinations = [];
  const combine = function (arr, m = []) {
    if (arr.length < 1) return combinations.push(m.join(''));
    for (let i = 0; i < arr.length; ++i) {
      const curr = arr.slice();
      const next = curr.splice(i, 1);
      combine(curr, m.concat(next));
    }
  };
  combine(words);

  const setMatch = function (regex, text, count = 0) {
    if (!text || text.length <= 0) return;
    regex.lastIndex = 0;
    while ((match = regex.exec(text)) != null) {
      const realIndex = match.index + count;
      if (!result.includes(realIndex)) {
        result.push(realIndex);
        const copy = text.split('');
        copy.splice(match.index, 1);
        const next = copy.join('');
        count++;

        setMatch(regex, next, count);
      }
      text = '';
    }
  };

  for (let c of combinations) {
    const regex = new RegExp(c, 'gi');
    setMatch(regex, str, 0);
  }
  return result;
}

function findSubstringOr(str, words) {
  const len = words.length;
  if (len < 1) return [];
  let pattern = words[0];
  console.log(words);
  console.log(pattern);
  for (let i = 1; i < len; ++i) {
    pattern += '|' + words[i];
  }
  const regex = new RegExp(pattern, 'gi');
  console.log('pattern: /', pattern);
  const result = [];
  while ((match = regex.exec(str)) != null) {
    result.push(match.index);
  }
  return result;
}

console.log(findSubstring('abcdoothefoobadcan', ['a', 'b', 'c', 'd']));
console.log(findSubstring('barfoothefoobarman', ['foo', 'bar']));
console.log(
  findSubstring('wordgoodgoodgoodbestword', ['word', 'good', 'best', 'word'])
);
// console.log(
//   findSubstring('wordgoodgoodgoodbestword', ['word', 'good', 'best', 'good'])
// );
// console.log(findSubstring('aaa', ['a', 'a']));
// console.log(findSubstring('aaaaaaaaaaaaaaaa', ['aa', 'aa']));
// console.log(
//   findSubstring(
//     'pjzkrkevzztxductzzxmxsvwjkxpvukmfjywwetvfnujhweiybwvvsrfequzkhossmootkmyxgjgfordrpapjuunmqnxxdrqrfgkrsjqbszgiqlcfnrpjlcwdrvbumtotzylshdvccdmsqoadfrpsvnwpizlwszrtyclhgilklydbmfhuywotjmktnwrfvizvnmfvvqfiokkdprznnnjycttprkxpuykhmpchiksyucbmtabiqkisgbhxngmhezrrqvayfsxauampdpxtafniiwfvdufhtwajrbkxtjzqjnfocdhekumttuqwovfjrgulhekcpjszyynadxhnttgmnxkduqmmyhzfnjhducesctufqbumxbamalqudeibljgbspeotkgvddcwgxidaiqcvgwykhbysjzlzfbupkqunuqtraxrlptivshhbihtsigtpipguhbhctcvubnhqipncyxfjebdnjyetnlnvmuxhzsdahkrscewabejifmxombiamxvauuitoltyymsarqcuuoezcbqpdaprxmsrickwpgwpsoplhugbikbkotzrtqkscekkgwjycfnvwfgdzogjzjvpcvixnsqsxacfwndzvrwrycwxrcismdhqapoojegggkocyrdtkzmiekhxoppctytvphjynrhtcvxcobxbcjjivtfjiwmduhzjokkbctweqtigwfhzorjlkpuuliaipbtfldinyetoybvugevwvhhhweejogrghllsouipabfafcxnhukcbtmxzshoyyufjhzadhrelweszbfgwpkzlwxkogyogutscvuhcllphshivnoteztpxsaoaacgxyaztuixhunrowzljqfqrahosheukhahhbiaxqzfmmwcjxountkevsvpbzjnilwpoermxrtlfroqoclexxisrdhvfsindffslyekrzwzqkpeocilatftymodgztjgybtyheqgcpwogdcjlnlesefgvimwbxcbzvaibspdjnrpqtyeilkcspknyylbwndvkffmzuriilxagyerjptbgeqgebiaqnvdubrtxibhvakcyotkfonmseszhczapxdlauexehhaireihxsplgdgmxfvaevrbadbwjbdrkfbbjjkgcztkcbwagtcnrtqryuqixtzhaakjlurnumzyovawrcjiwabuwretmdamfkxrgqgcdgbrdbnugzecbgyxxdqmisaqcyjkqrntxqmdrczxbebemcblftxplafnyoxqimkhcykwamvdsxjezkpgdpvopddptdfbprjustquhlazkjfluxrzopqdstulybnqvyknrchbphcarknnhhovweaqawdyxsqsqahkepluypwrzjegqtdoxfgzdkydeoxvrfhxusrujnmjzqrrlxglcmkiykldbiasnhrjbjekystzilrwkzhontwmehrfsrzfaqrbbxncphbzuuxeteshyrveamjsfiaharkcqxefghgceeixkdgkuboupxnwhnfigpkwnqdvzlydpidcljmflbccarbiegsmweklwngvygbqpescpeichmfidgsjmkvkofvkuehsmkkbocgejoiqcnafvuokelwuqsgkyoekaroptuvekfvmtxtqshcwsztkrzwrpabqrrhnlerxjojemcxel',
//     [
//       'dhvf',
//       'sind',
//       'ffsl',
//       'yekr',
//       'zwzq',
//       'kpeo',
//       'cila',
//       'tfty',
//       'modg',
//       'ztjg',
//       'ybty',
//       'heqg',
//       'cpwo',
//       'gdcj',
//       'lnle',
//       'sefg',
//       'vimw',
//       'bxcb',
//     ]
//   )
// );
