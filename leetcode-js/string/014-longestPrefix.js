function longestPrefix(strs) {
  let prefix = '';
  const len = strs.length;
  if (len <= 0) return '';
  if (len === 1) return strs[0];

  const str1 = strs[0];
  const rest = strs.slice(1, len);
  for (let c of str1) {
    const regex = new RegExp('^' + prefix + c, 'i');
    for (let str of rest) {
      if (!regex.test(str)) return prefix;
    }
    prefix += c;
  }
  return prefix;
}

console.log(longestPrefix(['flower', 'flow', 'flight']));
console.log(longestPrefix(['dog', 'racecar', 'car']));
