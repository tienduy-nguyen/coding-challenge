function longestPalindromicSubstring(str) {
  const len = str.length;
  let max = '';
  console.log('----------------------------------');
  console.log(`i    |j    |left   |right    |max`);
  console.log('----------------------------------');
  for (let i = 0; i < len; ++i) {
    for (let j = 0; j < 2; j++) {
      let left = i;
      let right = i + j;
      while (str[left] && str[left] == str[right]) {
        left--;
        right++;
      }
      if (right - left - 1 > max.length) {
        max = str.substring(left + 1, right);
      }
      console.log(
        `${i}    |${j}    |${
          left >= 0 ? '+' + left.toString() : left
        }     |${right}        |${max}`
      );
    }
  }
  return max;
}

// console.log(longestPalindromicSubstring('acabebacaf'));
// console.log(longestPalindromicSubstring('cbbd'));
// console.log(longestPalindromicSubstring('a'));
// console.log(longestPalindromicSubstring('ac'));
// console.log(longestPalindromicSubstring('bbb'));
// console.log(longestPalindromicSubstring('abbb'));
// console.log(longestPalindromicSubstring('abb'));
// console.log(longestPalindromicSubstring('aba'));
// console.log(longestPalindromicSubstring('abba'));

// Dynamic programing

function longestPalindrome(str) {
  const len = str.length;
  if (len <= 1) return str;

  // construct a 2D array
  const dp = [...new Array(len + 1)].map((_) => new Array(len + 1).fill(false));
  let result = '';

  // base case for one character
  for (let i = 0; i < len; ++i) {
    dp[i][i] = true;
    result = str[i];
  }

  // base case for two character
  for (let i = 0; i < str.length; ++i) {
    if (str[i] === str[i + 1]) dp[i][i + 1] = true;
    if (dp[i][i + 1]) result = str.substring(i, i + 2);
  }

  // expand to three or more character
  for (let i = len - 1; i >= 0; --i) {
    console.log('---i----', i);
    for (let j = i + 2; j < len; ++j) {
      console.log(`i - j : ${i} - ${j}`);
      dp[i][j] = dp[i + 1][j - 1] && str[i] === str[j];
      if (dp[i][j])
        result = result.length < j - i + 1 ? str.substring(i, j + 1) : result;
    }
  }
  console.log(dp);
  console.log(result);
  return result;
}

console.log(longestPalindrome('bbbdd'));
