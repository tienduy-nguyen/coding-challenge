function romanToInteger(str) {
  const map = { I: 1, V: 5, X: 10, L: 50, C: 100, D: 500, M: 1000 };
  let num = 0;

  for (let i = 0; i < str.length; ++i) {
    const curr = map[str[i]];
    const next = map[str[i + 1]];
    if (curr < next) num -= curr;
    else num += curr;
  }
  return num;
}

console.log(romanToInteger('LVIII'));
console.log(romanToInteger('MCMXCIV'));
