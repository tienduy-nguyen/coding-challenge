package main

func longestPalindrome(s string) string {
	result := ""
	max := 0
	for i := 0; i < len(s); i++ {
		for j := i + 1; j < len(s); j++ {
			str := s[i:(j + 1)]
			strReverse := reverse(str)
			if str == strReverse && len(str) > max {
				max = len(str)
				result = str
			}

		}
	}
	if max == 0 {
		return s[:1]
	}
	return result
}

func reverse(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

func main() {
	println(longestPalindrome("bb"))
}
