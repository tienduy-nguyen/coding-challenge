package main

import (
	"regexp"
)

func main() {
	println(isMatch("aa", "a"))
	println(isMatch("aa", "a*"))
	println(isMatch("ab", ".*"))
	println(isMatch("aab", "c*a*b"))
	println(isMatch("mississippi", "mis*is*p*."))
}

func isMatch(s string, p string) bool {
	re := regexp.MustCompile(p)
	match := re.FindAllString(s, -1)
	if match != nil && len(match[0]) == len(s) {
		return true
	}
	return false
}
