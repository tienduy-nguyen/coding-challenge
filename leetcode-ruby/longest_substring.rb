# Link of challenge
# https://leetcode.com/problems/longest-substring-without-repeating-characters/
# Given a string, find the length of the longest substring without repeating characters.

# @param {String} s
# @return {Integer}
# Native method
def length_of_longest_substring(str)
    max_len = 0;
    str_len = str.length
    if str_len <= 1
      return str_len
    end
    for i in 0..str_len-1
      check = []
      for j in i..str_len-1
        if !check.include?(str[j])
          check.push(str[j])
        else
          break
        end
      end
      if check.size > max_len
        max_len = check.size
      end
    end
    return max_len
end
puts length_of_longest_substring("fqdfjqkldfjqsmdfjqsdjld")
puts length_of_longest_substring("au")
puts length_of_longest_substring("aucdb")
puts length_of_longest_substring("pwwkew")
puts length_of_longest_substring("aab")
puts length_of_longest_substring("bbtablud")


# Reafactor_method
def length_of_longest_substring2(str)
  max = 0
  start_index= -1
  char_map = {}
  str.chars.each_with_index do |c,i|
    if current_index = char_map[c]  and current_index > start_index
      start_index = char_map[c]
    end
    char_map[c] = i
    new_len = i - start_index
    max = new_len > max ? new_len : max

  end
  return max
end
puts '-' * 100
puts length_of_longest_substring2("fqdfjqkldfjqsmdfjqsdjld")
puts length_of_longest_substring2("au")
puts length_of_longest_substring2("aucdb")
puts length_of_longest_substring2("pwwkew")
puts length_of_longest_substring2("aab")
puts length_of_longest_substring2("bbtablud")