# Complete the method/function so that it converts dash/underscore 
# delimited words into camel casing. 
# The first word within the output should be capitalized only 
# if the original word was capitalized 
# (known as Upper Camel Case, also often referred to as Pascal case).

# Example

# to_camel_case("the-stealth-warrior") # returns "theStealthWarrior"
# to_camel_case("The_Stealth_Warrior") # returns "TheStealthWarrior"

def to_camel_case(str)
  arr = str.split(/[-_]/)
  if arr.size <= 1
    return arr.join
  end
  first = arr[0]
  rest = arr[1..-1]
  return first+rest.map(&:capitalize).join
end


# Solution codewars
def to_camel_case2(str)
  str.gsub (/[_-](.)/) {"#{$1.upcase}"}
end
def to_camel_case3(str)
  str.gsub('_','-').split('-').each_with_index.map{ |x,i| i == 0 ? x : x.capitalize }.join
end
def to_camel_case4(str)
  head, *tail = str.split(/[-_]/)
  head.to_s + tail.map(&:capitalize).join
end
p to_camel_case2("the-stealth-warrior")
p to_camel_case2("The_Stealth_Warrior")
p to_camel_case2("The_")
p to_camel_case2("The_")
p to_camel_case2("")
p "The_Stealth_Warrior".gsub(/[_-](.)/)

