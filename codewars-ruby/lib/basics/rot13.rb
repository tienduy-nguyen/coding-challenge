# ROT13 is a simple letter substitution cipher that replaces a letter     end
# with the letter 13 letters after it in the alphabet. 
# ROT13 is an example of the Caesar cipher.

# Create a function that takes a string 
# and returns the string ciphered with Rot13. 
# If there are numbers or special characters included in the string, 
# they should be returned as they are. 
# Only letters from the latin/english alphabet should be shifted, 
# like in the original Rot13 "implementation".

# a-z: 97-122
# A-Z: 65-90

def rot13(str)
  lower=*(97..122)
  upper=*(65..90)
  str_ascii = str.bytes.map do |num|
    if lower.include?(num)
      (num-13) >= 97 ? (num-13) : (num-13+122-96)
    elsif upper.include?(num)
      (num-13) >= 65 ? (num-13) : (num-13+90-64)
    else
      num
    end
  end
  return str_ascii.map(&:chr).join
end
p rot13("Test")
p rot13("test")

# Solution codewars
def rot13_2(string)
  string.tr("A-Za-z", "N-ZA-Mn-za-m")
end
def rot13_3(string)
  origin = ('a'..'z').to_a.join + ('A'..'Z').to_a.join
  cipher = ('a'..'z').to_a.rotate(13).join + ('A'..'Z').to_a.rotate(13).join
  string.tr(origin, cipher)
end

p ('a'..'z').to_a.rotate(13).join
