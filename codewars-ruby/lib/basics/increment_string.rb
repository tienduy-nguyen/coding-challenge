def increment_string(input)
  if input.empty?
    return "1"
  end
  arr = input.split(/(\d*$)/)
  num = (arr[1].to_i+1).to_s
  new_num = (arr.size > 1) && num.length < arr[1].length ? ("0"*(arr[1].length-num.length)+num) : num
  return arr[0]+new_num
  # input.sub(/\d*$/) { |n| n.empty? ? 1 : n.next}
end

p increment_string("fo1o123")
p increment_string("fo1o")
p increment_string("fo1o001")
p increment_string("")
p increment_string("002")

# Solution codewars
def increment_string2(input)
  input.sub(/\d*$/) { |n| n.empty? ? 1 : n.succ }
end

def increment_string3(input)
  input[/\d\z/] ? input.sub(/(\d+)\z/) { $1.next } : input + '1'
end

def increment_string4(input)
  input.gsub(/\d+/) { |match| match.succ } if Integer(input.chars.last) rescue input << "1"
end