def score(dice)
  rules = {"31"=>1000,
    "36"=>600,
    "35"=>500,
    "34"=>400,
    "33"=>300,
    "32"=>200,
    "11"=>100,
    "21"=>200,
    "41"=>1100,
    "51"=>1200,
    "15"=>50,
    "25"=>100,
    "45"=>550,
    "55"=>600
  }

  arr_uniq=dice.uniq

  # Get a hash {"1" => 0, "2" =>0 ...}
  check = arr_uniq.zip(Array.new(arr_uniq.size,0)).to_h
  arr = dice.each do |num|
    if check.key?(num)
      check[num] += 1
    end
  end

  # Convert the hash to an array with the same format of rules
  new_arr =[]
  check.each {|k,v| 
    new_arr.push(v.to_s+k.to_s)
  }

  # solve the sum
  sum = 0
  new_arr.each do |item|
    if rules.key?(item)
      sum += rules[item]
    end
  end
  return sum
end


# p score([5,1,3,4,1]) #50
# p score([1,1,1,3,1]) #1100
# p score([2,4,4,5,4]) #450
# p score([2,3,4,6,2]) #0
# p score([2,2,2,3,3]) #200
# p score([2,2,3,3,3]) #300
# p score([3,2,6,3,3]) #300
# p score([]) #0

# Solution dev.to
RULES = [
  { v: 1, c: 3, p: 1000 },
  { v: 6, c: 3, p: 600 },
  { v: 5, c: 3, p: 500 },
  { v: 4, c: 3, p: 400 },
  { v: 3, c: 3, p: 300 },
  { v: 2, c: 3, p: 200 },
  { v: 1, c: 1, p: 100 },
  { v: 5, c: 1, p: 50 }
].freeze

def score2(arr)
  hash = arr.group_by(&:itself).map { |k,v| [k, v.count] }.to_h
  get_poinst(hash)
end

def get_poinst(hash)
  total = 0
  RULES.each do |rule|
    v, c, p = rule.map { |k, val| val }

    next if hash[v].nil?

    count = (hash[v] / c).floor
    hash[v] -= count * c
    total += count * p
  end
  total
end

# p [3,2,6,3,3].group_by(&:itself)
p [3,2,6,3,3].count(3)

# Solution codewars
SCORE_MAP = {
  1 => [0, 100, 200, 1000, 1100, 1200, 2000],
  2 => [0, 0, 0, 200, 200, 200, 400],
  3 => [0, 0, 0, 300, 300, 300, 600],
  4 => [0, 0, 0, 400, 400, 400, 800],
  5 => [0, 50, 100, 500, 550, 600, 1000],
  6 => [0, 0, 0, 600, 600, 600, 1200]
}

def score3( dice )
  (1..6).inject(0) do |score, die|
    score += SCORE_MAP[die][dice.count(die)]
  end
end

def score4( dice )
  [ dice.count(1) / 3 * 1000,
    dice.count(6) / 3 * 600,
    dice.count(5) / 3 * 500,
    dice.count(4) / 3 * 400,
    dice.count(3) / 3 * 300,
    dice.count(2) / 3 * 200,
    dice.count(1) % 3 * 100,
    dice.count(5) % 3 * 50 ].reduce(:+)
end

def score5( dice )
  m = {1 => 100, 5 => 50}
  (1..6).reduce(0) do|res, i|
    count = dice.count(i)    
    res + count/3 * i * (i==1 ? 1000 : 100) + count%3*(m[i].to_i)
  end
end