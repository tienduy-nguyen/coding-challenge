def alphabet_position(text)
  text=text.downcase.gsub(/[^a-z]/,'')
  text.bytes.map{|x| x-96}.join(' ')
end
puts alphabet_position("The sunset sets at twelve o' clock.")