# Codewar with Ruby

This is a serie of kata challenge using Ruby

### 7kyu
| #    | Kata Codewar                                                                      | Solution                                                                                                     |
| :--- | :-------------------------------------------------------------------------------- | :----------------------------------------------------------------------------------------------------------- |
| 1    | [Vowel Count](https://www.codewars.com/kata/54ff3102c1bad923760001f3)             | [Solution](https://github.com/tienduy-nguyen/thehackingproject/blob/master/week2/day5/lib/vowel_count.rb)    |
| 2    | [Remove the minium](https://www.codewars.com/kata/563cf89eb4747c5fb100001b)       | [Solution](https://github.com/tienduy-nguyen/thehackingproject/blob/master/week2/day5/lib/remove_minimum.rb) |
| 4    | [Get the middle caracter](https://www.codewars.com/kata/56747fd5cb988479af000028) | [Solution](https://github.com/tienduy-nguyen/thehackingproject/blob/master/week2/day5/lib/get_middle.rb)     |
| 5    | [Jade casing strings](https://www.codewars.com/kata/5390bac347d09b7da40006f6)     | [Solution](https://github.com/tienduy-nguyen/thehackingproject/blob/master/week2/day5/lib/to_jade_case.rb)   |
| 6    | [Disemvowel trolls](https://www.codewars.com/kata/52fba66badcd10859f00097e)       | [Solution](https://github.com/tienduy-nguyen/thehackingproject/blob/master/week2/day5/lib/disemvowel.rb)     |
| 7    | [Square every digit](https://www.codewars.com/kata/546e2562b03326a88e000020)      | [Solution](https://github.com/tienduy-nguyen/thehackingproject/blob/master/week2/day5/lib/square_digits.rb)  |
| 8    | [Shortest word](https://www.codewars.com/kata/57cebe1dc6fdc20c57000ac9)           | [Solution](https://github.com/tienduy-nguyen/thehackingproject/blob/master/week2/day5/lib/shortest_word.rb)  |
| 9    | [List filtering](https://www.codewars.com/kata/53dbd5315a3c69eed20002dd)          | [Solution](https://github.com/tienduy-nguyen/thehackingproject/blob/master/week2/day5/lib/filter_list.rb)    |
| 10   | [Credit card mark](https://www.codewars.com/kata/5412509bd436bd33920011bc)        | [Solution](https://github.com/tienduy-nguyen/thehackingproject/blob/master/week2/day5/lib/credit_card.rb)    |
| 11   | [Categorize new member](https://www.codewars.com/kata/5502c9e7b3216ec63c0001aa)   | [Solution](https://github.com/tienduy-nguyen/thehackingproject/blob/master/week2/day5/lib/open_or_senior.rb) |


### 6kyu
| #    | Kata Codewar                                                                                        | Solution                                                                                                          |
| :--- | :-------------------------------------------------------------------------------------------------- | :---------------------------------------------------------------------------------------------------------------- |
| 1    | [Replace with alphabet position](https://www.codewars.com/kata/546f922b54af40e1e90001da/train/ruby) | [Solution](https://github.com/tienduy-nguyen/coding-challenge/blob/master/codewars-ruby/lib/alphabet_position.rb) |
| 2    | [Array.diff](https://www.codewars.com/kata/523f5d21c841566fde000009/train/ruby)                     | [Solution](https://github.com/tienduy-nguyen/coding-challenge/blob/master/codewars-ruby/lib/array_diff.rb)        |
| 4    | [Weird case](https://www.codewars.com/kata/52b757663a95b11b3d00062d/solutions/ruby)                 | [Solution](https://github.com/tienduy-nguyen/coding-challenge/blob/master/codewars-ruby/lib/weirdcase.rb)         |
| 5    | [Find the parity oulier](https://www.codewars.com/kata/5526fc09a1bbd946250002dc)                    | [Solution](https://github.com/tienduy-nguyen/thehackingproject/blob/master/week2/day5/lib/find_outlier.rb)        |
| 6    | ...                                                                                                 | [Solution]()                                                                                                      |
| 7    | ...                                                                                                 | [Solution]()                                                                                                      |
| 8    | ...                                                                                                 | [Solution]()                                                                                                      |
| 9    | ...                                                                                                 | [Solution]()                                                                                                      |
| 10   | ...                                                                                                 | [Solution]()                                                                                                      |
| 11   | ...                                                                                                 | [Solution]()                                                                                                      |
| 12   | ...                                                                                                 | [Solution]()                                                                                                      |
| 13   | ...                                                                                                 | [Solution]()                                                                                                      |


### 5kyu
| #    | Kata Codewar                                                                            | Solution                                                                                                           |
| :--- | :-------------------------------------------------------------------------------------- | :----------------------------------------------------------------------------------------------------------------- |
| 1    | [Rot13](https://www.codewars.com/kata/530e15517bc88ac656000716/ruby)                    | [Solution](https://github.com/tienduy-nguyen/coding-challenge/blob/master/codewars-ruby/lib/rot13.rb)              |
| 2    | [Greed is good](https://www.codewars.com/kata/5270d0d18625160ada0000e4/train/ruby)      | [Solution](https://github.com/tienduy-nguyen/coding-challenge/blob/master/codewars-ruby/lib/greed_is_good.rb)      |
| 4    | [String incrementer](https://www.codewars.com/kata/54a91a4883a7de5d7800009c/train/ruby) | [Solution](https://github.com/tienduy-nguyen/coding-challenge/blob/master/codewars-ruby/lib/string_incrementer.rb) |
| 5    | ...                                                                                     | [Solution]()                                                                                                       |
| 6    | ...                                                                                     | [Solution]()                                                                                                       |
| 7    | ...                                                                                     | [Solution]()                                                                                                       |
| 8    | ...                                                                                     | [Solution]()                                                                                                       |
| 9    | ...                                                                                     | [Solution]()                                                                                                       |
| 10   | ...                                                                                     | [Solution]()                                                                                                       |
| 11   | ...                                                                                     | [Solution]()                                                                                                       |
| 12   | ...                                                                                     | [Solution]()                                                                                                       |
| 13   | ...                                                                                     | [Solution]()                                                                                                       |