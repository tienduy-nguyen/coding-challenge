require_relative '../lib/to_camel_case.rb'

describe "Solution" do
  it "should test for something" do
    expect(to_camel_case("the-stealth-warrior")).to eq("theStealthWarrior")
    expect(to_camel_case("The_Stealth_Warrior")).to eq("TheStealthWarrior")
    expect(to_camel_case("the_")).to eq("the")
    expect(to_camel_case("The-")).to eq("The")
    expect(to_camel_case("")).to eq("")
  end
end
