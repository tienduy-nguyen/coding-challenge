require_relative '../lib/weirdcase.rb'

describe 'weirdcase' do
  it 'should return the correct value for a single word' do
    expect(weirdcase('This')).to eq('ThIs')
    expect(weirdcase('is')).to eq('Is')
  end
  it 'should return the correct value for multiple words' do
    expect(weirdcase('This is a test')).to eq('ThIs Is A TeSt')
  end
end