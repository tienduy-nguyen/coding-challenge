require_relative '../lib/array_diff.rb'

describe "array_diff" do
  it "Should work for example" do
    expect(array_diff([1,2], [1])).to eq([2])
    expect(array_diff([1,2,2], [1])).to eq([2,2])
    expect(array_diff([1,2,2], [2])).to eq([1])
    expect(array_diff([1,2,2], [])).to eq([1,2,2])
    expect(array_diff([], [1,2])).to eq([])
  end
  
end
