require_relative('../lib/greed_is_good.rb')

describe "Scorer Function" do
  it "should value this as worthless" do
    expect( score( [2, 3, 4, 6, 2] )).to eq(0)
  end
  
  it "should value this triplet correctly" do
    expect( score( [2, 2, 2, 3, 3] )).to eq(200)
  end
  
  it "should value this mixed set correctly" do
    expect( score( [2, 4, 4, 5, 4] )).to eq(450)
  end
end